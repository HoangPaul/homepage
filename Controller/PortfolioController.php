<?php

require_once("../Lib/Validate.php");
require_once("../Lib/UploadFile.php");

class PortfolioController extends AppController {
    public $helpers = array('Html', 'Form');
    public $components = array(
        'Session', 
        'Auth'=> array(
            /*
            'loginAction' => array(
                'controller' => 'portfolio', 
                'action' => 'index'
            ),
             */
            'loginRedirect' => array(
                'controller' => 'portfolio',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'portfolio',
                'action' => 'index',
                'home'
            ),
        )
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'view');
    }

    public function index() {
        $options['fields'] = array('DISTINCT Portfolio.id', 'Portfolio.summary', 'Portfolio.title');

        if ($this->request->is('post') && !empty($this->data['tags'])) {
            $options['joins'] = array(
                array(
                    'table' => 'portfolios_tags',
                    'alias' => 'PortfolioTag',
                    'type' => 'inner',
                    'conditions' => array(
                        'Portfolio.id = PortfolioTag.portfolio_id'
                    )
                )
            );
            $options['conditions'] = array(
                'PortfolioTag.tag_id' => $this->data['tags']
            );
            $options['group'] = array(
                'Portfolio.id HAVING COUNT(*)='.count($this->data['tags'])
            );
        }

        $tags = $this->Portfolio->Tag->find('list');

        /* Count the number of portfolio items that belong to a tag and append it */
        $count_tags = $this->Portfolio->Tag->find('all');
        foreach ($count_tags as $tag) {
            $count = count($tag['Portfolio']);
            $tags[$tag['Tag']['id']] .= " ({$count})";
        }

        $this->set('portfolio', $this->Portfolio->find('all', $options));
        $this->set('tags', $tags);
    }

    public function view($id = null) {
        if (!$id) {
            $this->redirect(array('action' => 'index'));
        }
        $data = $this->Portfolio->findById($id);
        if (!$data) {
            throw new NotFoundException(__('Portfolio item not found'));
        }
        $this->set('portfolio', $data);

    }

    public function add() {
        $this->set('tags', $this->Portfolio->Tag->find('list'));
        if ($this->request->is('post')) {
            //$this->uploadFile();
            //return;
            //die();

            $this->Portfolio->create();
            if ($this->Portfolio->saveAll($this->request->data)) {
                $this->Session->setFlash(__('Your portfolio has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to add your portfolio.'));
            //debug($this->Portfolio->validationErrors); die();
        }
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid portfolio'));
        }
        $portfolio = $this->Portfolio->findById($id);
        $this->set('tags', $this->Portfolio->Tag->find('list'));
        if (!$portfolio) {
            throw new NotFoundException(__('Portfolio not found'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Portfolio->id = $id;
            if ($this->Portfolio->save($this->request->data)) {
                $this->Session->setFlash(__('Your portfolio has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to update your portflio.'));
        }

        if (!$this->request->data) {
            $this->request->data = $portfolio;
        }
        
    }

    public function thumbnaila($id = null) {
        $this->response->file('uploads/big_icon.png');
        $this->response->header('Content-Disposition', 'inline');
        return $this->response;
    }

    function uploadFile() {
        if (Validate::isValidImageFile($this->request->data['Portfolio']['file'])) {
            UploadFile::move_uploaded_image_safe($this->request->data['Portfolio']['file']);
            echo "true";
        } else {
            echo "false";
        }
    }
}

?>
