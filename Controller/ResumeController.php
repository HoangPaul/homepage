<?php

class ResumeController extends AppController {
    public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('resume', $this->Resume->find('all'));
    }

    public function view_pdf($id = null) {
        $resume = $this->Resume->findById($id);
        if (!$resume) {
            throw new NotFoundException(__('Resume not found'));
        }
        $this->response->file($resume['Resume']['url']);
        $this->response->header('Content-Disposition', 'inline');
        return $this->response;
    }
}

?>

