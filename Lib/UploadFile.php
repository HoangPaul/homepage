<?php

/* Assumes the files have already been validated */
class UploadFile {


    /* Derived from http://php.net/manual/en/features.file-upload.php by CertaiN */
    static function move_uploaded_file_safe($file) {
        // You should name it uniquely.
        // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $newFilePath = sprintf('../uploads/%s.%s', sha1_file($file['tmp_name']), $ext);
        if (!move_uploaded_file(
            $file['tmp_name'],
            $newFilePath
        )) {
            throw new RuntimeException('Failed to move uploaded file.');
        }
        echo "asdasd"; 
        return $newFilePath;
    }

    /* Derived from http://php.net/manual/en/features.file-upload.php by CertaiN */
    static function move_uploaded_image_safe($file) {
        // You should name it uniquely.
        // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
            $finfo->file($file['tmp_name']),
            array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
            ),
            true
        )) {
        echo "extension is " . $ext;
            throw new RuntimeException('Invalid file format.');
        }
        $newFilePath = sprintf('../uploads/%s.%s', sha1_file($file['tmp_name']), $ext);
        echo "extension is " . $ext;
        if (!move_uploaded_file(
            $file['tmp_name'],
            $newFilePath
        )) {
            throw new RuntimeException('Failed to move uploaded file.');
        }
        return $newFilePath;
    }

}

?>
