<?php

class Portfolio extends AppModel {
    public $name = 'Portfolio';
    public $useTable = 'portfolios';
    public $hasMany = array(
        'Image' => array (
            'className'         => 'Image',
            'foreignKey'        => 'portfolio_id',
            'dependent'         => true
        )
    );
    public $hasAndBelongsToMany = array(
            'Tag' =>
                array(
                    'className'             => 'Tag',
                    'joinTable'             => 'portfolios_tags',
                    'foreignKey'            => 'portfolio_id',
                    'associationForeignKey' => 'tag_id',
                )
    );
}

?>
