<?php

class Image extends AppModel {
    public $name = 'Image';
    public $useTable = 'images';
    public $displayField = 'url';
    public $belongsTo = array(
        'Portfolio' => array(
            'className' => 'Portfolio',
            'foreignKey' => 'portfolio_id'
        )
    );

}

?>
