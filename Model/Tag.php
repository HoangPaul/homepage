<?php

class Tag extends AppModel {
    public $name = 'Tag';
    public $useTable = 'tags';
    public $displayField = 'name';
    public $hasAndBelongsToMany = array(
            'Portfolio' =>
                array(
                    'className'             => 'Portfolio',
                    'joinTable'             => 'portfolios_tags',
                    'foreignKey'            => 'tag_id',
                    'associationForeignKey' => 'portfolio_id',
                )
            );
}

?>
