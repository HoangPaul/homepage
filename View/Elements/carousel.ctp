<h2> Images </h2>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="height:500px;">
    <?php $numImages = count($portfolio['Image'])?>
    <!-- Indicators -->
    <ol class="carousel-indicators">
    <?php
        $isActive = true;
        for ($i = 0; $i < $numImages; $i++) {
            echo '<li data-target="#carousel-example-generic" data-slide-to="'.$i.'"';
            if ($isActive) {
                echo ' class="active"';
            } 
            echo '></li>';
            $isActive = false;
        } 
    ?>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php 
            $isActive = true;
            foreach($portfolio['Image'] as $img) {
                echo $this->element('carousel-item',
                    array(
                        'img' => $img,
                        'isActive' => $isActive
                    )
                );
                $isActive = false;
            }
        ?>
    </div>
    <!-- Controls -->
    <?php
        if ($numImages > 1) {
            echo '
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>';
        }
    ?>
</div>

