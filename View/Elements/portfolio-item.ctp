<?php
App::uses('Parsedown', 'Lib');
$Parsedown = new Parsedown();
?>

<div class="row">
    <div class="page-header">
    <h3>
        <?php echo $this->Html->link(
            $title,
            array(
                'controller' => 'portfolio',
                'action' => 'view',
                $id
            )
        )?>
    </h3>
    <!-- Glyphicon button link, edit portfolio item -->
    <?php
        if ($this->Session->read('Auth.User')) {
            echo $this->Html->link(
                '<button type="button" class="btn btn-default" aria-label="Left Align">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                 </button>',
                array (
                    'controller' => 'portfolio',
                    'action' => 'edit',
                    $id
                ),
                array('escape' => false)
            );
        }
    ?>
    </div>
    <!--
    <div class="col-xs-4 col-md-3">
        <a href="#" class="thumbnail">
            <img data-src="holder.js/170x170" alt="...">
        </a>
    </div>
    -->
    <div class="col-xs-8 col-md-12">
        <p>
        <?php echo $Parsedown->text($summary); ?>
        <br />
        <p> Tags: <?php 
        foreach($tags as $tag) { 
            echo '<span class="label label-default">' . $tag['name']. '</span> ';
        } ?> </p>
    </div>
</div>

