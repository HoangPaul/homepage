<?php $this->layout = 'website';
App::uses('Parsedown', 'Lib');
$Parsedown = new Parsedown();

?>

<div class="container"  style="height:calc(100% - 170px);">
    <div class="jumbotron">
        <h1> Welcome </h1>
        <p> I put my projects here. Look around.</p>
        <br />
        <br />
        <?php echo $Parsedown->text("## About me
####(February 1994 - Ongoing)   
####Solo Project   

I'm a recent graduate of RMIT with a Bachelor of Computer Science. I specialise in programming with particular emphasis on good design, data structures and efficient algorithms, although I do dabble in other areas such as front-end design, web security and team management.  

Outside programming, I enjoy playing video games (especially Super Smash Bros. Melee and DOTA 2), photography and long walks on the beach. My aim is to continue learning and improve my craft. 
")?>

    </div>
</div>

