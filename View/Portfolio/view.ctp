<?php
$this->extend('common');

App::uses('Parsedown', 'Lib');
$Parsedown = new Parsedown();


?>

    <div class="container"  style="height:calc(100% - 170px);">
        <?php echo $Parsedown->text($portfolio['Portfolio']['body']); ?>
        <?php
            if (isset($portfolio['Image'][0])) {
                echo $this->element('carousel', 
                    array(
                        'portfolio' => $portfolio
                    )
                );
            }
        ?>
        <br />
        <p>
            <?php
                if ($portfolio['Portfolio']['git_url']) {
                    if (strpos($portfolio['Portfolio']['git_url'], 'github')) {
                        echo '<i class="fa fa-github"></i> ';
                        echo $this->Html->link(
                        'GitHub respository',
                        $portfolio['Portfolio']['git_url'],
                        array('escape' => false)
                    );
                    } else {
                        echo '<i class="fa fa-bitbucket"></i> ';
                        echo $this->Html->link(
                        'Bitbucket respository',
                        $portfolio['Portfolio']['git_url'],
                        array('escape' => false)
                    );
                    }
                    
                }
            ?>
        </p>
        <p>
        Tags: 
        <?php
            foreach($portfolio['Tag'] as $tag) {
                echo '<span class="label label-default">' . $tag['name']. '</span> ';
            }
        ?>           
        </p>
	</div>
