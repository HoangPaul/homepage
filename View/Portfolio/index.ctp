<?php 
echo $this->extend('common');
$this->start('more-scripts');
echo $this->Html->script(array('sidebar-search'));
$this->end();
?>

<div class="container">
    <div class="page-header" style="margin-top:10px">
        <h1>Portfolio</h1>

    </div>

    <div class="row">
        <div class="col-xs-12 col-md-9" style="padding-right:60px">
            <!-- Glyphicon button link, add portfolio item -->
            <?php
                if ($this->Session->read('Auth.User')) {
                    echo $this->Html->link(
                        '<button type="button" class="btn btn-default" aria-label="Left Align">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                         </button>',
                        array (
                            'controller' => 'portfolio',
                            'action' => 'add'
                        ),
                        array('escape' => false)
                    );
                }
            ?>

            <!-- Portfolio item -->
            <?php   
                foreach($portfolio as $item) {
                    echo $this->element('portfolio-item',
                        array(
                                'id' => $item['Portfolio']['id'],
                                'title' => $item['Portfolio']['title'],
                                'summary' => $item['Portfolio']['summary'],
                                'tags' => $item['Tag']
                        )
                    );
                }
            ?>
        </div>
        <!-- Sidebar -->
        <div class="col-xs-12 col-md-3 sidebar">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h5>
                                Tags
                        </h5>
                    </div>
                    </a>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <?php
                                echo $this->Form->create('Portfolio');
                                echo $this->Form->input('tags', 
                                    array(
                                        'name' => 'tags',
                                        'label' => false,
                                        'type' => 'select',
                                        'multiple' => 'checkbox',
                                        'selected' => isset($this->data['tags'])?$this->data['tags']:null,
                                        'options' => $tags
                                    )
                                );
                            ?>
                        </div>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <?php
                                echo $this->Form->end('Filter');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <!--
            <p>
                Many proejcts that I've done over the years can be found on this page.
            </p>
            -->
        </div>
    </div>
</div>

<script src="http://imsky.github.com/holder/holder.js"></script>
