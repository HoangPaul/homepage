<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Paul's website</title>
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
        <meta name="msapplication-TileColor" content="#da532c">

        <?php echo $this->Html->css(array('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css')); ?>
        <?php echo $this->Html->css(array('bootstrap.min', 'styles' )); ?>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NKSPSK"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NKSPSK');</script>
        <!-- End Google Tag Manager -->
        <div class="header">
            <nav class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">Paul Hoang</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <?php
                                echo $this->element('navbar_item', 
                                array(
                                        'title' => 'Portfolio',
                                        'controller' => 'portfolio',
                                        'isActive' => isset($navbarPortfolio)
                                    )
                                );
                                echo $this->element('navbar_item', 
                                    array(
                                        'title' => 'Resume',
                                        'controller' => 'resume',
                                        'isActive' => isset($navbarResume)
                                    )
                                );
                                echo $this->element('navbar_item', 
                                    array(
                                        'title' => 'Contact',
                                        'controller' => 'contact',
                                        'isActive' => isset($navbarContact)
                                    )
                                );
                            ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <?php echo $this->Session->flash(); ?>
	    <?php echo $this->fetch('content'); ?>

        <?php echo $this->Html->script(array('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js')) ?>
        <?php echo $this->Html->script(array('bootstrap', 'footer'))?>
        <?php echo $this->fetch('more-scripts'); ?>
    </body>
</html>
