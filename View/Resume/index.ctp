<?php
$this->extend('common');
?>

<div class="container"  style="height:calc(100% - 170px);">
	<div class="page-header" style="margin-top:10px">
        <h1>Resume</h1>
    </div>

    <div class="row" style="height:100%">
        <div class="col-xs-12 col-md-9" style="height:100%; padding-right:60px;">
            <div class="row" style="height:100%">
      		    <div class="embed-responsive" style="height:100%; " >
     			    <iframe class="embed-responsive-item" type="application/pdf" src="resume/view_pdf/2" width="100%" height="100%" >
         		</iframe>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-md-3" style="background-color:#f5f5f5; margin-top:20px;padding-top:10px">
            <p> My generic resume. </p>
        </div>
    </div>
</div>

