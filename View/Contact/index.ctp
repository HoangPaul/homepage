<?php
$this->extend('common');
?>
    <div class="container"  style="height:calc(100% - 170px);">
		<div class="page-header" style="margin-top:10px">
			<h1>Contact</h1>
        </div>
        <p>
            For any enquiries, please email me.
        </p>
        <p>
            <i class="fa fa-envelope-o"></i> PaulKhoaHoang {at} gmail.com
        </p>
        <p>
        <i class="fa fa-github"></i> 
        <?php
            echo $this->Html->link(
                'GitHub respository',
                'http://www.github.com/HoangPaul/',
                array('escape' => false)
            );
        ?>
        </p>
        <p>
        <i class="fa fa-bitbucket"></i> 
        <?php
            echo $this->Html->link(
                'Bitbucket respository',
                'http://www.bitbucket.org/HoangPaul/',
                array('escape' => false)
            );
        ?>
        </p>
	</div>

